do_install_append() {
    # do not autostart xinput calibration
    rm -f ${D}${sysconfdir}/X11/Xsession.d/30xinput_calibrate.sh
    rm -f ${D}${sysconfdir}/xdg/autostart/xinput_calibrator.desktop
}
