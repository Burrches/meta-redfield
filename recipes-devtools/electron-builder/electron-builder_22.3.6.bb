# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

SUMMARY = "A complete solution to package and build a ready for distribution Electron app for MacOS, Windows and Linux with “auto update” support out of the box"
HOMEPAGE = "https://github.com/electron-userland/electron-builder"
# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
#
# The following license files were not able to be identified and are
# represented as "Unknown" below, you will need to check them yourself:
#   node_modules/http-cache-semantics/LICENSE
#   node_modules/duplexer3/LICENSE.md
#   node_modules/rc/LICENSE.BSD
#   node_modules/rc/LICENSE.APACHE2
#   node_modules/dotenv/LICENSE
#   node_modules/json5/LICENSE.md
#   node_modules/app-builder-lib/templates/nsis/empty-license.txt
#   node_modules/app-builder-lib/electron-osx-sign/LICENSE
#   node_modules/app-builder-lib/out/util/license.d.ts
#   node_modules/app-builder-lib/out/util/license.js
#   node_modules/app-builder-lib/out/util/license.js.map
#   node_modules/app-builder-lib/out/targets/nsis/nsisLicense.js
#   node_modules/app-builder-lib/out/targets/nsis/nsisLicense.js.map
#   node_modules/app-builder-lib/out/targets/nsis/nsisLicense.d.ts
#   node_modules/@types/yargs/LICENSE
#   node_modules/@types/debug/LICENSE
#   node_modules/@types/fs-extra/LICENSE
#   node_modules/@types/node/LICENSE
#   node_modules/@types/yargs-parser/LICENSE
#   node_modules/esprima/LICENSE.BSD
#   node_modules/configstore/license
#   node_modules/normalize-package-data/LICENSE
#   node_modules/uri-js/LICENSE
#   node_modules/balanced-match/LICENSE.md
#   node_modules/update-notifier/license
#   node_modules/sprintf-js/LICENSE
#   node_modules/source-map/LICENSE
#   node_modules/sanitize-filename/LICENSE.md
#   node_modules/sax/LICENSE
#   node_modules/dotenv-expand/LICENSE
#   node_modules/dmg-builder/out/licenseButtons.d.ts
#   node_modules/dmg-builder/out/licenseDefaultButtons.d.ts
#   node_modules/dmg-builder/out/dmgLicense.d.ts
#   node_modules/dmg-builder/out/licenseButtons.js.map
#   node_modules/dmg-builder/out/dmgLicense.js
#   node_modules/dmg-builder/out/licenseDefaultButtons.js.map
#   node_modules/dmg-builder/out/licenseButtons.js
#   node_modules/dmg-builder/out/dmgLicense.js.map
#   node_modules/dmg-builder/out/licenseDefaultButtons.js
#
# NOTE: multiple licenses have been detected; they have been separated with &
# in the LICENSE value for now since it is a reasonable assumption that all
# of the licenses apply. If instead there is a choice between the multiple
# licenses then you should change the value to separate the licenses with |
# instead of &. If there is any doubt, check the accompanying documentation
# to determine which situation is applicable.
LICENSE = "MIT & Unknown & Apache-2.0 & ISC"
LIC_FILES_CHKSUM = "file://node_modules/async-exit-hook/license;md5=a12ebca0510a773644101a99a867d210 \
                    file://node_modules/iconv-lite/LICENSE;md5=f942263d98f0d75e0e0101884e86261d \
                    file://node_modules/http-cache-semantics/LICENSE;md5=7b7cd412797b9e24e3c58eff96661bf9 \
                    file://node_modules/ejs/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57 \
                    file://node_modules/is-fullwidth-code-point/license;md5=a12ebca0510a773644101a99a867d210 \
                    file://node_modules/keyv/LICENSE;md5=9a2baa6947d5018096b83508bd94026e \
                    file://node_modules/mime/LICENSE;md5=8e8ea2ad138ce468f8570a0edbadea65 \
                    file://node_modules/clone-response/LICENSE;md5=9a2baa6947d5018096b83508bd94026e \
                    file://node_modules/duplexer3/LICENSE.md;md5=2ac3dfdce5a77f9cff9b5f70d216d17d \
                    file://node_modules/yallist/LICENSE;md5=82703a69f6d7411dde679954c2fd9dca \
                    file://node_modules/rc/LICENSE.MIT;md5=e0f70a42adf526e6f5e605a94d98a420 \
                    file://node_modules/rc/LICENSE.BSD;md5=e7a2a325a0069e82aff675bbf74464a0 \
                    file://node_modules/rc/LICENSE.APACHE2;md5=ffcf739dca268cb0f20336d6c1a038f1 \
                    file://node_modules/jsonfile/LICENSE;md5=423f377ade95936f6fe009b1c137bfdb \
                    file://node_modules/escape-goat/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/cliui/LICENSE.txt;md5=83623193d3051ca8068a89a455c699ca \
                    file://node_modules/cliui/node_modules/ansi-regex/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/cliui/node_modules/strip-ansi/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/safer-buffer/LICENSE;md5=3baebc2a17b8f5bff04882cd0dc0f76e \
                    file://node_modules/responselike/LICENSE;md5=ebe53e6698981352da17c09ed915f13f \
                    file://node_modules/import-lazy/license;md5=a12ebca0510a773644101a99a867d210 \
                    file://node_modules/has-flag/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/ci-info/LICENSE;md5=0c6fa9c682ce05a9a4da4f4a96cf42ec \
                    file://node_modules/dotenv/LICENSE;md5=f063f692e0722821de5bd48ee5898746 \
                    file://node_modules/isbinaryfile/LICENSE.txt;md5=4bdcb081801209a698dd9dc80acccf8b \
                    file://node_modules/minimist/LICENSE;md5=aea1cde69645f4b99be4ff7ca9abcce1 \
                    file://node_modules/chalk/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/yargs/LICENSE;md5=ec88e65c7bb01732069f3ad6b09ac90d \
                    file://node_modules/json5/LICENSE.md;md5=d80f2808a405d641840b50a06f80e93c \
                    file://node_modules/graceful-fs/LICENSE;md5=fd63805fd8e3797063b247781e5ee6e4 \
                    file://node_modules/concat-map/LICENSE;md5=aea1cde69645f4b99be4ff7ca9abcce1 \
                    file://node_modules/got/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/string-width/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/string-width/node_modules/is-fullwidth-code-point/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/string-width/node_modules/emoji-regex/LICENSE-MIT.txt;md5=ee9bd8b835cfcd512dd644540dd96987 \
                    file://node_modules/string-width/node_modules/ansi-regex/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/string-width/node_modules/strip-ansi/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/7zip-bin/LICENSE.txt;md5=d9fb291501a56892c48e0808076b7037 \
                    file://node_modules/pupa/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/is-obj/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/json-schema-traverse/LICENSE;md5=ea87ade09b9e6da4f2e47904a4ee137b \
                    file://node_modules/ajv-keywords/LICENSE;md5=417f4a53535e2142253a61d7829dd0ad \
                    file://node_modules/find-up/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/has-yarn/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/app-builder-lib/templates/nsis/empty-license.txt;md5=8524de963f07201e5c086830d370797f \
                    file://node_modules/app-builder-lib/electron-osx-sign/LICENSE;md5=fee67d282726f24cf1ac4289410365b9 \
                    file://node_modules/app-builder-lib/out/util/license.d.ts;md5=4593864e0d54d9d3f78d9780e70a92ce \
                    file://node_modules/app-builder-lib/out/util/license.js;md5=22a97f164728132e327be0c7c57f6b9d \
                    file://node_modules/app-builder-lib/out/util/license.js.map;md5=76a3694fad5ebbd87a582f5390315baf \
                    file://node_modules/app-builder-lib/out/targets/nsis/nsisLicense.js;md5=9e4200088b192d9ce824d48ef58fde47 \
                    file://node_modules/app-builder-lib/out/targets/nsis/nsisLicense.js.map;md5=cd32f5de1fbc1771777e728d6ca1a969 \
                    file://node_modules/app-builder-lib/out/targets/nsis/nsisLicense.d.ts;md5=7719a8fd23d737f417f47c92da0136c5 \
                    file://node_modules/p-locate/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/global-dirs/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/@types/yargs/LICENSE;md5=d4a904ca135bb7bc912156fee12726f0 \
                    file://node_modules/@types/debug/LICENSE;md5=27e94c0280987ab296b0b8dd02ab9fe5 \
                    file://node_modules/@types/fs-extra/LICENSE;md5=d4a904ca135bb7bc912156fee12726f0 \
                    file://node_modules/@types/node/LICENSE;md5=d4a904ca135bb7bc912156fee12726f0 \
                    file://node_modules/@types/yargs-parser/LICENSE;md5=27e94c0280987ab296b0b8dd02ab9fe5 \
                    file://node_modules/cacheable-request/LICENSE;md5=9a2baa6947d5018096b83508bd94026e \
                    file://node_modules/cacheable-request/node_modules/lowercase-keys/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/cacheable-request/node_modules/get-stream/license;md5=d5f2a6dd0192dcc7c833e50bb9017337 \
                    file://node_modules/is-path-inside/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/deep-extend/LICENSE;md5=827bb5781213ff1e9d2fe309bbfc0115 \
                    file://node_modules/esprima/LICENSE.BSD;md5=e3c825c932a984958bafbac21e21f984 \
                    file://node_modules/package-json/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/package-json/node_modules/semver/LICENSE;md5=82703a69f6d7411dde679954c2fd9dca \
                    file://node_modules/ansi-align/LICENSE;md5=42624e59bd3dabedcca59e1b54e3af50 \
                    file://node_modules/ansi-align/node_modules/string-width/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/configstore/license;md5=2838f6acc24e1d83ccb791d68f311c29 \
                    file://node_modules/path-exists/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/spdx-correct/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57 \
                    file://node_modules/require-directory/LICENSE;md5=903997d26b12b72b43aa60018387bb90 \
                    file://node_modules/normalize-package-data/LICENSE;md5=745d0cb7803882ab26451fefb149878e \
                    file://node_modules/normalize-package-data/node_modules/hosted-git-info/LICENSE;md5=a01f10cd299d5727263720d47cc2c908 \
                    file://node_modules/normalize-package-data/node_modules/semver/LICENSE;md5=82703a69f6d7411dde679954c2fd9dca \
                    file://node_modules/defer-to-connect/LICENSE;md5=78fd36a7284eb85369521bcb6e863186 \
                    file://node_modules/locate-path/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/ansi-styles/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/debug/LICENSE;md5=ddd815a475e7338b0be7a14d8ee35a99 \
                    file://node_modules/uri-js/LICENSE;md5=3b55dad4a98748003b5b423477713da1 \
                    file://node_modules/balanced-match/LICENSE.md;md5=7fa99ddc3424107350ca6e9a24552085 \
                    file://node_modules/stat-mode/LICENSE;md5=45c7470c6a8ec7844f9125c5c49b82d5 \
                    file://node_modules/lowercase-keys/license;md5=a12ebca0510a773644101a99a867d210 \
                    file://node_modules/crypto-random-string/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/async/LICENSE;md5=dc113e0fc4029c29942399ad22425402 \
                    file://node_modules/fast-json-stable-stringify/LICENSE;md5=febe55307df96f60ad763842f5a8ca6f \
                    file://node_modules/hosted-git-info/LICENSE;md5=a01f10cd299d5727263720d47cc2c908 \
                    file://node_modules/universalify/LICENSE;md5=a734c6ad6e37a515025ac5e8e90ef786 \
                    file://node_modules/ini/LICENSE;md5=82703a69f6d7411dde679954c2fd9dca \
                    file://node_modules/fast-deep-equal/LICENSE;md5=ea87ade09b9e6da4f2e47904a4ee137b \
                    file://node_modules/fs-extra/LICENSE;md5=ea817882455c03503f7d014a8f54f095 \
                    file://node_modules/make-dir/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/make-dir/node_modules/semver/LICENSE;md5=82703a69f6d7411dde679954c2fd9dca \
                    file://node_modules/jake/node_modules/has-flag/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/jake/node_modules/chalk/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/jake/node_modules/ansi-styles/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/jake/node_modules/color-name/LICENSE;md5=d301869b39e08b33665b7c4f16b8e41d \
                    file://node_modules/jake/node_modules/supports-color/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/jake/node_modules/color-convert/LICENSE;md5=9bdadfc9fbb3ab8d5a6d591bdbd52811 \
                    file://node_modules/once/LICENSE;md5=82703a69f6d7411dde679954c2fd9dca \
                    file://node_modules/signal-exit/LICENSE.txt;md5=e29e20260a1c78dba16a233048565cde \
                    file://node_modules/p-try/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/update-notifier/license;md5=893ad33659c31c56c0f4ba00ac978281 \
                    file://node_modules/p-cancelable/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/@develar/schema-utils/LICENSE;md5=95a881ed5cb29fc8a0fa0356525f30ac \
                    file://node_modules/is-typedarray/LICENSE.md;md5=aea1cde69645f4b99be4ff7ca9abcce1 \
                    file://node_modules/sprintf-js/LICENSE;md5=ab40451de4e35c90f1b663b4fbd314a9 \
                    file://node_modules/semver/LICENSE;md5=82703a69f6d7411dde679954c2fd9dca \
                    file://node_modules/semver-diff/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/semver-diff/node_modules/semver/LICENSE;md5=82703a69f6d7411dde679954c2fd9dca \
                    file://node_modules/get-caller-file/LICENSE.md;md5=d21aa422a43e9693d50143d612b58967 \
                    file://node_modules/emoji-regex/LICENSE-MIT.txt;md5=ee9bd8b835cfcd512dd644540dd96987 \
                    file://node_modules/url-parse-lax/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/ms/license.md;md5=fd56fd5f1860961dfa92d313167c37a6 \
                    file://node_modules/set-blocking/LICENSE.txt;md5=8fd106383180f7bbb8f534414fdf7d35 \
                    file://node_modules/prepend-http/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/require-main-filename/LICENSE.txt;md5=8fd106383180f7bbb8f534414fdf7d35 \
                    file://node_modules/source-map/LICENSE;md5=b1ca6dbc0075d56cbd9931a75566cd44 \
                    file://node_modules/is-ci/LICENSE;md5=0c6fa9c682ce05a9a4da4f4a96cf42ec \
                    file://node_modules/decompress-response/license;md5=5b422d6bf88afe88977d04f8bdd4959c \
                    file://node_modules/wrappy/LICENSE;md5=82703a69f6d7411dde679954c2fd9dca \
                    file://node_modules/end-of-stream/LICENSE;md5=9befe7026bf915886cd566a98117c80e \
                    file://node_modules/ansi-regex/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/get-stream/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/typedarray-to-buffer/LICENSE;md5=badd5e91c737e7ffdf10b40c1f907761 \
                    file://node_modules/pump/LICENSE;md5=9befe7026bf915886cd566a98117c80e \
                    file://node_modules/y18n/LICENSE;md5=55e5f88040679148136545002139a8b1 \
                    file://node_modules/js-yaml/LICENSE;md5=effd621a9bf5d72d6a7e6ef819bf3afb \
                    file://node_modules/color-name/LICENSE;md5=d301869b39e08b33665b7c4f16b8e41d \
                    file://node_modules/sanitize-filename/LICENSE.md;md5=73ea9bdf7c3ef73023bb243ac21965a3 \
                    file://node_modules/which-module/LICENSE;md5=42624e59bd3dabedcca59e1b54e3af50 \
                    file://node_modules/type-fest/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/is-installed-globally/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/registry-url/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/dot-prop/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/p-limit/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/bluebird/LICENSE;md5=5dd221993f51ba714ddd208ff3b2ed01 \
                    file://node_modules/punycode/LICENSE-MIT.txt;md5=ee9bd8b835cfcd512dd644540dd96987 \
                    file://node_modules/to-readable-stream/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/write-file-atomic/LICENSE;md5=68705ba56afdf6710c9187ed9a9cdd03 \
                    file://node_modules/@szmarczak/http-timer/LICENSE;md5=78fd36a7284eb85369521bcb6e863186 \
                    file://node_modules/argparse/LICENSE;md5=a2f2590d8d82b0fa2b3e2fc5c69d2b81 \
                    file://node_modules/sax/LICENSE;md5=326d5674181c4bb210e424772c60fa80 \
                    file://node_modules/spdx-expression-parse/LICENSE;md5=35a411d082d4487ab0e0287014cddf80 \
                    file://node_modules/decamelize/license;md5=a12ebca0510a773644101a99a867d210 \
                    file://node_modules/wrap-ansi/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/wrap-ansi/node_modules/ansi-regex/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/wrap-ansi/node_modules/strip-ansi/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/dotenv-expand/LICENSE;md5=3b69b5c7218905f4327651b959e2f748 \
                    file://node_modules/json-buffer/LICENSE;md5=f26ea3f2a31273ebd199d933804f5e8b \
                    file://node_modules/normalize-url/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/minimatch/LICENSE;md5=82703a69f6d7411dde679954c2fd9dca \
                    file://node_modules/supports-color/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/path-parse/LICENSE;md5=4b940f9668dfcb796d2cb98ad94692df \
                    file://node_modules/ajv/LICENSE;md5=4973982316cdc12e988b814af2813df7 \
                    file://node_modules/latest-version/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/buffer-from/LICENSE;md5=46513463e8f7d9eb671a243f0083b2c6 \
                    file://node_modules/camelcase/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/escape-string-regexp/license;md5=a12ebca0510a773644101a99a867d210 \
                    file://node_modules/resolve/LICENSE;md5=baa47288b5bd3e657a01886ce3dd0cb6 \
                    file://node_modules/term-size/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/unique-string/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/registry-auth-token/LICENSE;md5=7b1df94b672916450d194cf7a8bd0e2d \
                    file://node_modules/xdg-basedir/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/boxen/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/is-npm/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/cli-boxes/license;md5=d5f2a6dd0192dcc7c833e50bb9017337 \
                    file://node_modules/brace-expansion/LICENSE;md5=a5df515ef062cc3affd8c0ae59c059ec \
                    file://node_modules/widest-line/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/mimic-response/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/dmg-builder/out/licenseButtons.d.ts;md5=9b21695937980158e56827f325ae5c59 \
                    file://node_modules/dmg-builder/out/licenseDefaultButtons.d.ts;md5=42f1d73fb417778f3cbe1e3d880a2536 \
                    file://node_modules/dmg-builder/out/dmgLicense.d.ts;md5=de9ca416dc62b5d87a6da646feaa5344 \
                    file://node_modules/dmg-builder/out/licenseButtons.js.map;md5=dfd1557e780377b1937848f8ea879dc5 \
                    file://node_modules/dmg-builder/out/dmgLicense.js;md5=a1ce5dbb436261798e4528a72474e8dc \
                    file://node_modules/dmg-builder/out/licenseDefaultButtons.js.map;md5=8380d572dc19ee8d919ae7048aa85a54 \
                    file://node_modules/dmg-builder/out/licenseButtons.js;md5=2c9c471a344824e024aae5b758f10a83 \
                    file://node_modules/dmg-builder/out/dmgLicense.js.map;md5=361a96fc39d9a5c6ed2e73fb6aa98139 \
                    file://node_modules/dmg-builder/out/licenseDefaultButtons.js;md5=7947d471313a383325ff51c3d6f9a5b4 \
                    file://node_modules/strip-json-comments/license;md5=a12ebca0510a773644101a99a867d210 \
                    file://node_modules/strip-ansi/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/@sindresorhus/is/license;md5=915042b5df33c31a6db2b37eadaa00e3 \
                    file://node_modules/color-convert/LICENSE;md5=9bdadfc9fbb3ab8d5a6d591bdbd52811 \
                    file://node_modules/validate-npm-package-license/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57 \
                    file://node_modules/yargs-parser/LICENSE.txt;md5=8fd106383180f7bbb8f534414fdf7d35 \
                    file://node_modules/lru-cache/LICENSE;md5=82703a69f6d7411dde679954c2fd9dca \
                    file://node_modules/is-yarn-global/LICENSE;md5=70e5c3ddb07f13a85007f15c0792b800 \
                    file://node_modules/source-map-support/LICENSE.md;md5=f433e270f6b1d088c38b279d53048f5e \
                    file://package.json;md5=cff00d419985b60e1a864d30a9258f41 \
                    file://node_modules/7zip-bin/package.json;md5=30e18ec9806445b67484ace4f6077102 \
                    file://node_modules/@develar/schema-utils/package.json;md5=ab701c28887dcf77dc5296c976e1852a \
                    file://node_modules/@sindresorhus/is/package.json;md5=548474b692ee38c82cbeca71236efed8 \
                    file://node_modules/@szmarczak/http-timer/package.json;md5=b03ce7bc4ada130b03e4cef11f3cb93a \
                    file://node_modules/@types/debug/package.json;md5=5fee73f04c0da657937fa61b6d692eaa \
                    file://node_modules/@types/fs-extra/package.json;md5=a92dd7c4dce5080299da8406ec909047 \
                    file://node_modules/@types/node/package.json;md5=1dd6928c25eff2a47157dfc1eab2dd38 \
                    file://node_modules/@types/yargs/package.json;md5=27c343121849f7bf5b3e2e74cb627d11 \
                    file://node_modules/@types/yargs-parser/package.json;md5=61cba256f19f0006065c81560fa10e57 \
                    file://node_modules/ajv/package.json;md5=bc185fb380e6969e7ae607dde40bc9c3 \
                    file://node_modules/ajv-keywords/package.json;md5=6a43e82bf6a761d4896b7c593c4786e6 \
                    file://node_modules/ansi-align/node_modules/string-width/package.json;md5=4209467d763d22ace9e585bc5cf182b8 \
                    file://node_modules/ansi-align/package.json;md5=b67270dbe21422d84b1fc42a0908209e \
                    file://node_modules/ansi-regex/package.json;md5=cfe61795ca2de5f036f51c30cdca8e9a \
                    file://node_modules/ansi-styles/package.json;md5=ab7a71ab9f6b46acda83106c5f34e6e3 \
                    file://node_modules/app-builder-bin/package.json;md5=9dbb1a39d20f0938b67f90b6dde9d0c4 \
                    file://node_modules/app-builder-lib/package.json;md5=e9c8103feb75588c65480d4952d078c1 \
                    file://node_modules/argparse/package.json;md5=2bc829c736c7071f36ec6e08cd41ccfe \
                    file://node_modules/async/package.json;md5=45127fb25079e1e4237fdb9a6e7c71ae \
                    file://node_modules/async-exit-hook/package.json;md5=d7090ed35d0affc4f9a212348d2f1c6d \
                    file://node_modules/balanced-match/package.json;md5=a2181cb8cab70df2084e1bdce79a389b \
                    file://node_modules/bluebird/package.json;md5=b553710e014be64b2dd8e8704279e93d \
                    file://node_modules/bluebird-lst/package.json;md5=722ab27570683a9162dbd503cee6369a \
                    file://node_modules/boxen/package.json;md5=6d6509cec9bd66c7d3199dbcbc4d908a \
                    file://node_modules/brace-expansion/package.json;md5=effd91994b1b7ddb8a33060ad4541e6a \
                    file://node_modules/buffer-from/package.json;md5=d708da6e9719b6b3a221aedc7e1cbf92 \
                    file://node_modules/builder-util/package.json;md5=1ca638d1ac0f3b9aabcf643375bb6002 \
                    file://node_modules/builder-util-runtime/package.json;md5=d891dea39494067c1dd5b9e8a125b3ec \
                    file://node_modules/cacheable-request/node_modules/get-stream/package.json;md5=3fa281fa1d3786d084a422edc3ff4151 \
                    file://node_modules/cacheable-request/node_modules/lowercase-keys/package.json;md5=69dd1891d8c5331eb36e8a37e02e0778 \
                    file://node_modules/cacheable-request/package.json;md5=0ef4f84e82f8876e54d70935142ed47a \
                    file://node_modules/camelcase/package.json;md5=31d66d63d07c9d3ab05757ac887e555b \
                    file://node_modules/chalk/package.json;md5=8f1e7b27e1b505b58fc7710bcbd5f590 \
                    file://node_modules/chromium-pickle-js/package.json;md5=05337c07f8be3e497854c6ed1ac34e3d \
                    file://node_modules/ci-info/package.json;md5=a9abb9620c493e5e0f2a1b1e17c74fa5 \
                    file://node_modules/cli-boxes/package.json;md5=a19fac2e8e7d5b43c0cefc7588740a92 \
                    file://node_modules/cliui/node_modules/ansi-regex/package.json;md5=4ef3224a1d5ee3f0768bc26d95d38b26 \
                    file://node_modules/cliui/node_modules/strip-ansi/package.json;md5=f31547d4f0d779a9dc6a135b074c9154 \
                    file://node_modules/cliui/package.json;md5=7b96a725cd748174032a1ee492a1bef7 \
                    file://node_modules/clone-response/package.json;md5=cb61499ec6e3f1dfd78833f829ff9f9c \
                    file://node_modules/color-convert/package.json;md5=370b0177fdb4368f0a688ffa48559ecf \
                    file://node_modules/color-name/package.json;md5=ef649e8b7be42bba6d4fa34aca7e126a \
                    file://node_modules/concat-map/package.json;md5=85d8a674998927862b17adef4aa6a7b1 \
                    file://node_modules/configstore/package.json;md5=37f044880a6cacb1c92c48d1e4eb793e \
                    file://node_modules/crypto-random-string/package.json;md5=b91a08e82ca473401aae76ffe5b1275d \
                    file://node_modules/debug/package.json;md5=923be017f4e97d24c734b4f8f6405abb \
                    file://node_modules/decamelize/package.json;md5=494b4faeb16e12979e988b49de7684d6 \
                    file://node_modules/decompress-response/package.json;md5=e9d0d8f2f2861c919bacbdef0557ce61 \
                    file://node_modules/deep-extend/package.json;md5=843d8bcf451f015c3a6b3930e0b6eaee \
                    file://node_modules/defer-to-connect/package.json;md5=7fb92358ef650f0d12295a405da7f11b \
                    file://node_modules/dmg-builder/package.json;md5=fefd546e421b8ee41d604484eeddcaf1 \
                    file://node_modules/dot-prop/package.json;md5=e12a8222d7fbe7fdd4df6f37e9f321d2 \
                    file://node_modules/dotenv/package.json;md5=43a33999e5ae34095014d78106707bff \
                    file://node_modules/dotenv-expand/package.json;md5=fbb873b9e01ff12c792f439ad065e2b9 \
                    file://node_modules/duplexer3/package.json;md5=92ecdfdad9b868fdbfea4e89cecb7045 \
                    file://node_modules/ejs/package.json;md5=8c64a921ba5accca3015a091ff7a5745 \
                    file://node_modules/electron-publish/package.json;md5=bcac2923239d4428501b0fb3cd3d2b88 \
                    file://node_modules/emoji-regex/package.json;md5=266d18038de0a4e87ac1ba8fa95e7e73 \
                    file://node_modules/end-of-stream/package.json;md5=36be3e35e3740830415138528c4237d4 \
                    file://node_modules/escape-goat/package.json;md5=2f11efaa039ae210246a73d9852da812 \
                    file://node_modules/escape-string-regexp/package.json;md5=6050cf06c06dcb38dce670ff96b21aa9 \
                    file://node_modules/esprima/package.json;md5=0394ebc64b1f0c98e6b1e3fd65c2ee37 \
                    file://node_modules/fast-deep-equal/package.json;md5=da1199afd6b2d7a1eaa9e30acd35f4e6 \
                    file://node_modules/fast-json-stable-stringify/package.json;md5=903a31da36d2259011d342196a9b1265 \
                    file://node_modules/filelist/package.json;md5=e2c0825d2315c2e169690f5b71956721 \
                    file://node_modules/find-up/package.json;md5=2beafc739bd0176c5dc63281986cde7d \
                    file://node_modules/fs-extra/package.json;md5=2a77c0109b701ca127dec0a43d39dcbe \
                    file://node_modules/get-caller-file/package.json;md5=1b4e6c1baf2c3673b9a7d1cc649ee66a \
                    file://node_modules/get-stream/package.json;md5=a9999627668552d0afc9661299ee601c \
                    file://node_modules/global-dirs/package.json;md5=13114aa134c5fdd2583c14e018194f74 \
                    file://node_modules/got/package.json;md5=e13fead2d76fde01c3b8132681f7cf41 \
                    file://node_modules/graceful-fs/package.json;md5=b31cf400f270df9d7173307ac050e826 \
                    file://node_modules/has-flag/package.json;md5=636dbe2c8d513ad850070def501122bf \
                    file://node_modules/has-yarn/package.json;md5=69d1f028351eacb76dbf791fc2bbe413 \
                    file://node_modules/hosted-git-info/package.json;md5=4bb452cb8941159f7acd4b733211016c \
                    file://node_modules/http-cache-semantics/package.json;md5=d4c9abfdfc05dafaea38481b3fde6b0c \
                    file://node_modules/iconv-lite/package.json;md5=82ceca44e1f4219d31b0baf4dd48fbb4 \
                    file://node_modules/import-lazy/package.json;md5=037abcd7d20cdb8ad4ed2913181f89b2 \
                    file://node_modules/imurmurhash/package.json;md5=feb3f37f4780f79e5fdb5ff0870f1057 \
                    file://node_modules/ini/package.json;md5=c817b5b8913b3ab535d215aad84c3a7f \
                    file://node_modules/is-ci/package.json;md5=1f51093fe4865ae9449536af4c4f3c39 \
                    file://node_modules/is-fullwidth-code-point/package.json;md5=e199d7053a4c1508b8654c3965a58b20 \
                    file://node_modules/is-installed-globally/package.json;md5=0784ac84d0b3eb6798848986e017bc47 \
                    file://node_modules/is-npm/package.json;md5=a9a93a582c868ebc7458a8f21bde5f49 \
                    file://node_modules/is-obj/package.json;md5=bb96ec27362c03cabcd030b1507f8a2b \
                    file://node_modules/is-path-inside/package.json;md5=55f116afd7936582c06487ea49c46ce3 \
                    file://node_modules/is-typedarray/package.json;md5=018697ad65588671c2bdd7b3ec2bdef3 \
                    file://node_modules/is-yarn-global/package.json;md5=cf1ccb224fec890624932675ebcf9736 \
                    file://node_modules/isbinaryfile/package.json;md5=05bb7adf8446ad344fb6f25d16ee2cf3 \
                    file://node_modules/jake/node_modules/ansi-styles/package.json;md5=50f97f15be9377b1110c7a68f76fff0f \
                    file://node_modules/jake/node_modules/chalk/package.json;md5=a21f60da989d4bf1cd5396df6c638a98 \
                    file://node_modules/jake/node_modules/color-convert/package.json;md5=51a202f6ab9b016f0f7219701f1f5e70 \
                    file://node_modules/jake/node_modules/color-name/package.json;md5=7599aecb8597ca603c711d49a83dab59 \
                    file://node_modules/jake/node_modules/has-flag/package.json;md5=73cc8d1e96c19ce85a7abb8f9468a86c \
                    file://node_modules/jake/node_modules/supports-color/package.json;md5=79d88f702d689c72ebe1798d2853a96e \
                    file://node_modules/jake/package.json;md5=836342d9c8a07f4052278c569a47373f \
                    file://node_modules/js-yaml/package.json;md5=522ce1185213a73a4f0c93444e18ee9f \
                    file://node_modules/json-buffer/package.json;md5=3eecc3a34ec249a1ed41a2d601ccc200 \
                    file://node_modules/json-schema-traverse/package.json;md5=e706b186146b3f00005442861f98c091 \
                    file://node_modules/json5/package.json;md5=c3c6a43ea9349efac3c8024f34884207 \
                    file://node_modules/jsonfile/package.json;md5=72842a2c930b8d09d5e6f996fd8fe5e4 \
                    file://node_modules/keyv/package.json;md5=88886d75cfe8da49f1791933c7198be5 \
                    file://node_modules/latest-version/package.json;md5=8eda82d13069bc3dd788fb6130a50a2b \
                    file://node_modules/lazy-val/package.json;md5=7755068e30f39a3712461f37d308d9df \
                    file://node_modules/locate-path/package.json;md5=fe53da175b2ea65bd1708529b7437e87 \
                    file://node_modules/lowercase-keys/package.json;md5=178ca96ff779a0ac1a60d2b50b81956c \
                    file://node_modules/lru-cache/package.json;md5=5facce3b29b46c909e27bd642d950a3f \
                    file://node_modules/make-dir/node_modules/semver/package.json;md5=c2e73d8791a66c63b7b6030da5de03ff \
                    file://node_modules/make-dir/package.json;md5=44f774eeff7eb2385ef03c6f2798c79d \
                    file://node_modules/mime/package.json;md5=00599248f153c4d31594c84e5a6897bf \
                    file://node_modules/mimic-response/package.json;md5=d66371a4941413af2e7c6dbc3000b670 \
                    file://node_modules/minimatch/package.json;md5=b763d93b18d070a6449399d2e92d8c32 \
                    file://node_modules/minimist/package.json;md5=84505571ecc56b8071068f44de7c79b2 \
                    file://node_modules/ms/package.json;md5=b3ea7267a23f72028e774742792b114a \
                    file://node_modules/normalize-package-data/node_modules/hosted-git-info/package.json;md5=d9b6f36d7701b75d6c1c6bf6a5783e0d \
                    file://node_modules/normalize-package-data/node_modules/semver/package.json;md5=735f6ede2a644d292066a9813d8b9da6 \
                    file://node_modules/normalize-package-data/package.json;md5=ef1b0b1c2f14d0c0b61b193ead64bc5b \
                    file://node_modules/normalize-url/package.json;md5=298e72d36e8ac8a2a2cddf9306c79a90 \
                    file://node_modules/once/package.json;md5=afb6ea3bdcad6397e11a71615bd06e3b \
                    file://node_modules/p-cancelable/package.json;md5=73c15cd3de301e219d902a96059cf53e \
                    file://node_modules/p-limit/package.json;md5=05ea6bd0b1f47d88b1328a020729de28 \
                    file://node_modules/p-locate/package.json;md5=f4bd5aebe35587fade4e331c175c4846 \
                    file://node_modules/p-try/package.json;md5=c3ec2260c0b640c43f15e99e4fa7b593 \
                    file://node_modules/package-json/node_modules/semver/package.json;md5=c2e73d8791a66c63b7b6030da5de03ff \
                    file://node_modules/package-json/package.json;md5=8f09d58c4ac07fd669e899d73dcdbc77 \
                    file://node_modules/path-exists/package.json;md5=99ba5e8952c0bb3471b6b1611a0fc534 \
                    file://node_modules/path-parse/package.json;md5=63b66a2ecf520db6b1a3246bafd6f4bc \
                    file://node_modules/prepend-http/package.json;md5=925f902d67d9809ec3728170fd6ba150 \
                    file://node_modules/pump/package.json;md5=aaae9cf98124b7abea552bfa6b4ec7d5 \
                    file://node_modules/punycode/package.json;md5=11e1d3e03bb34de07e247a480cebb0b0 \
                    file://node_modules/pupa/package.json;md5=917745b01025f0c8916d2b1bf5849615 \
                    file://node_modules/rc/package.json;md5=dcf8f74e9fad2b9d45a0c5d70eba335d \
                    file://node_modules/read-config-file/package.json;md5=f94be043d66eee3f800e525ad83f2d3d \
                    file://node_modules/registry-auth-token/package.json;md5=e413bb054ec7f17eddb8967003604663 \
                    file://node_modules/registry-url/package.json;md5=4f969baf5d71826e6741878abf8100f1 \
                    file://node_modules/require-directory/package.json;md5=77d2cd39b67ccdf73bfc037d229cf0f8 \
                    file://node_modules/require-main-filename/package.json;md5=cb18c83a456ed5f4fdefcf20bbad4296 \
                    file://node_modules/resolve/package.json;md5=32a1b5e1e08fa2264cf703dc0c500708 \
                    file://node_modules/responselike/package.json;md5=fe1842e785e64364e0b29ed0f1177f48 \
                    file://node_modules/safer-buffer/package.json;md5=274d956f400350c9f6cf96d22cdda227 \
                    file://node_modules/sanitize-filename/package.json;md5=5e00cee441c7ac5d016338ecd00c4cad \
                    file://node_modules/sax/package.json;md5=4f338f842e93421a35e3ec9e051d650b \
                    file://node_modules/semver/package.json;md5=9c8e85514fe8e9c88606a5227b4028f3 \
                    file://node_modules/semver-diff/node_modules/semver/package.json;md5=c2e73d8791a66c63b7b6030da5de03ff \
                    file://node_modules/semver-diff/package.json;md5=b141f3d514f414aecbfa87e4b66ebf58 \
                    file://node_modules/set-blocking/package.json;md5=e37224b4c865b4464d6d41b1f8a870a4 \
                    file://node_modules/signal-exit/package.json;md5=37c7ccfe57d64fc58b6c352c0cd59083 \
                    file://node_modules/source-map/package.json;md5=5f7feb368962c3130c5accf22ffd047c \
                    file://node_modules/source-map-support/package.json;md5=6a8932761d99e7d465d691bd4bfab6ab \
                    file://node_modules/spdx-correct/package.json;md5=29e279d6d5aba760045511de60b01cad \
                    file://node_modules/spdx-exceptions/package.json;md5=39574483a6be9b11884d0cd2812d3cef \
                    file://node_modules/spdx-expression-parse/package.json;md5=9da13b6bbcd9c2ec5782fe1b0c0d795e \
                    file://node_modules/spdx-license-ids/package.json;md5=369b262afa7cb3f55d5257cde8389232 \
                    file://node_modules/sprintf-js/package.json;md5=403904b398a71123fc7c5173dfbe5ffe \
                    file://node_modules/stat-mode/package.json;md5=217c7fd9d01dadd4b434bfd2d5098cbd \
                    file://node_modules/string-width/node_modules/ansi-regex/package.json;md5=4ef3224a1d5ee3f0768bc26d95d38b26 \
                    file://node_modules/string-width/node_modules/emoji-regex/package.json;md5=57308bb3048f9db51fb79d3354f5a06f \
                    file://node_modules/string-width/node_modules/is-fullwidth-code-point/package.json;md5=7c2dfa1f539b955d64d6af55282e1d9e \
                    file://node_modules/string-width/node_modules/strip-ansi/package.json;md5=f31547d4f0d779a9dc6a135b074c9154 \
                    file://node_modules/string-width/package.json;md5=1f4a09b8543c80895ac76a7b14530c60 \
                    file://node_modules/strip-ansi/package.json;md5=a162133463d6bd8b4c87a45158e7846f \
                    file://node_modules/strip-json-comments/package.json;md5=1dfad7430c94d2f136604def66ed9854 \
                    file://node_modules/supports-color/package.json;md5=9bd5f75e8324cc8e0b633c59f1c4b60b \
                    file://node_modules/temp-file/package.json;md5=a30b201b7bde5e0a1f6b8335eca89406 \
                    file://node_modules/term-size/package.json;md5=0e564b7c92ca773e4c50731de0b2396e \
                    file://node_modules/to-readable-stream/package.json;md5=cdf7a3833dc4a683796eccfca2a5bfb6 \
                    file://node_modules/truncate-utf8-bytes/package.json;md5=e887f3ec7bbeaade6ad927bb1790faad \
                    file://node_modules/type-fest/package.json;md5=6d4049152bc0ad3ee4db0c73a0a659e4 \
                    file://node_modules/typedarray-to-buffer/package.json;md5=869689681cc4810bae2dc7eebaa1fc39 \
                    file://node_modules/unique-string/package.json;md5=c146eae173d09099095648c4a1bc69fd \
                    file://node_modules/universalify/package.json;md5=2083a560cd169722497c2af0741a428b \
                    file://node_modules/update-notifier/package.json;md5=97a4f2fbda78c8cd79ede7f6026bee46 \
                    file://node_modules/uri-js/package.json;md5=ed6d85a25ab0dfd8a6c9a63442f9b77e \
                    file://node_modules/url-parse-lax/package.json;md5=05aeb8afc1984d1b1b1e672f7797d953 \
                    file://node_modules/utf8-byte-length/package.json;md5=17b53432145d0de345a8d2513767648a \
                    file://node_modules/validate-npm-package-license/package.json;md5=e8168fa784142fe417f82359aeffc444 \
                    file://node_modules/which-module/package.json;md5=b4ab6a39c5e86263ade16635797218d2 \
                    file://node_modules/widest-line/package.json;md5=6bf73a261527eca398fe7da28b7eb4f7 \
                    file://node_modules/wrap-ansi/node_modules/ansi-regex/package.json;md5=4ef3224a1d5ee3f0768bc26d95d38b26 \
                    file://node_modules/wrap-ansi/node_modules/strip-ansi/package.json;md5=f31547d4f0d779a9dc6a135b074c9154 \
                    file://node_modules/wrap-ansi/package.json;md5=0bd9ef9e79c628202bf1edf7a38a8052 \
                    file://node_modules/wrappy/package.json;md5=788804d507f3ed479ea7614fa7d3f1a5 \
                    file://node_modules/write-file-atomic/package.json;md5=4a1f8da627526dc7847c62c83c139035 \
                    file://node_modules/xdg-basedir/package.json;md5=606fa2dbd1c89e801804df35e73a9a96 \
                    file://node_modules/y18n/package.json;md5=978ec516289533908a9523ce2aad8d9c \
                    file://node_modules/yallist/package.json;md5=b15d27bf2cf04083fef9389ce68aa620 \
                    file://node_modules/yargs/package.json;md5=46ef436134f32cde79d1de2ac8a24558 \
                    file://node_modules/yargs-parser/package.json;md5=8b0c628b94396c7d183d1f184a5f4b93"

SRC_URI = " \
    npm://registry.npmjs.org/;package=electron-builder;version=${PV} \
    npmsw://${THISDIR}/${BPN}/npm-shrinkwrap.json;dev=1 \
    "

S = "${WORKDIR}/npm"

inherit npm

LICENSE_${PN} = "Unknown"
LICENSE_${PN}-7zip-bin = "MIT"
LICENSE_${PN}-develar-schema-utils = "MIT"
LICENSE_${PN}-sindresorhus-is = "MIT"
LICENSE_${PN}-szmarczak-http-timer = "MIT"
LICENSE_${PN}-types-debug = "Unknown"
LICENSE_${PN}-types-fs-extra = "Unknown"
LICENSE_${PN}-types-node = "Unknown"
LICENSE_${PN}-types-yargs = "Unknown"
LICENSE_${PN}-types-yargs-parser = "Unknown"
LICENSE_${PN}-ajv = "MIT"
LICENSE_${PN}-ajv-keywords = "MIT"
LICENSE_${PN}-ansi-align-string-width = "MIT"
LICENSE_${PN}-ansi-align = "ISC"
LICENSE_${PN}-ansi-regex = "MIT"
LICENSE_${PN}-ansi-styles = "MIT"
LICENSE_${PN}-app-builder-bin = "Unknown"
LICENSE_${PN}-app-builder-lib = "Unknown"
LICENSE_${PN}-argparse = "MIT"
LICENSE_${PN}-async = "MIT"
LICENSE_${PN}-async-exit-hook = "MIT"
LICENSE_${PN}-balanced-match = "Unknown"
LICENSE_${PN}-bluebird = "MIT"
LICENSE_${PN}-bluebird-lst = "Unknown"
LICENSE_${PN}-boxen = "MIT"
LICENSE_${PN}-brace-expansion = "MIT"
LICENSE_${PN}-buffer-from = "MIT"
LICENSE_${PN}-builder-util = "Unknown"
LICENSE_${PN}-builder-util-runtime = "Unknown"
LICENSE_${PN}-cacheable-request-get-stream = "MIT"
LICENSE_${PN}-cacheable-request-lowercase-keys = "MIT"
LICENSE_${PN}-cacheable-request = "MIT"
LICENSE_${PN}-camelcase = "MIT"
LICENSE_${PN}-chalk = "MIT"
LICENSE_${PN}-chromium-pickle-js = "Unknown"
LICENSE_${PN}-ci-info = "MIT"
LICENSE_${PN}-cli-boxes = "MIT"
LICENSE_${PN}-cliui-ansi-regex = "MIT"
LICENSE_${PN}-cliui-strip-ansi = "MIT"
LICENSE_${PN}-cliui = "ISC"
LICENSE_${PN}-clone-response = "MIT"
LICENSE_${PN}-color-convert = "MIT"
LICENSE_${PN}-color-name = "MIT"
LICENSE_${PN}-concat-map = "MIT"
LICENSE_${PN}-configstore = "Unknown"
LICENSE_${PN}-crypto-random-string = "MIT"
LICENSE_${PN}-debug = "MIT"
LICENSE_${PN}-decamelize = "MIT"
LICENSE_${PN}-decompress-response = "MIT"
LICENSE_${PN}-deep-extend = "MIT"
LICENSE_${PN}-defer-to-connect = "MIT"
LICENSE_${PN}-dmg-builder = "Unknown"
LICENSE_${PN}-dot-prop = "MIT"
LICENSE_${PN}-dotenv = "Unknown"
LICENSE_${PN}-dotenv-expand = "Unknown"
LICENSE_${PN}-duplexer3 = "Unknown"
LICENSE_${PN}-ejs = "Apache-2.0"
LICENSE_${PN}-electron-publish = "Unknown"
LICENSE_${PN}-emoji-regex = "MIT"
LICENSE_${PN}-end-of-stream = "MIT"
LICENSE_${PN}-escape-goat = "MIT"
LICENSE_${PN}-escape-string-regexp = "MIT"
LICENSE_${PN}-esprima = "Unknown"
LICENSE_${PN}-fast-deep-equal = "MIT"
LICENSE_${PN}-fast-json-stable-stringify = "MIT"
LICENSE_${PN}-filelist = "Unknown"
LICENSE_${PN}-find-up = "MIT"
LICENSE_${PN}-fs-extra = "MIT"
LICENSE_${PN}-get-caller-file = "ISC"
LICENSE_${PN}-get-stream = "MIT"
LICENSE_${PN}-global-dirs = "MIT"
LICENSE_${PN}-got = "MIT"
LICENSE_${PN}-graceful-fs = "ISC"
LICENSE_${PN}-has-flag = "MIT"
LICENSE_${PN}-has-yarn = "MIT"
LICENSE_${PN}-hosted-git-info = "ISC"
LICENSE_${PN}-http-cache-semantics = "Unknown"
LICENSE_${PN}-iconv-lite = "MIT"
LICENSE_${PN}-import-lazy = "MIT"
LICENSE_${PN}-imurmurhash = "Unknown"
LICENSE_${PN}-ini = "ISC"
LICENSE_${PN}-is-ci = "MIT"
LICENSE_${PN}-is-fullwidth-code-point = "MIT"
LICENSE_${PN}-is-installed-globally = "MIT"
LICENSE_${PN}-is-npm = "MIT"
LICENSE_${PN}-is-obj = "MIT"
LICENSE_${PN}-is-path-inside = "MIT"
LICENSE_${PN}-is-typedarray = "MIT"
LICENSE_${PN}-is-yarn-global = "MIT"
LICENSE_${PN}-isbinaryfile = "MIT"
LICENSE_${PN}-jake-ansi-styles = "MIT"
LICENSE_${PN}-jake-chalk = "MIT"
LICENSE_${PN}-jake-color-convert = "MIT"
LICENSE_${PN}-jake-color-name = "MIT"
LICENSE_${PN}-jake-has-flag = "MIT"
LICENSE_${PN}-jake-supports-color = "MIT"
LICENSE_${PN}-jake = "Unknown"
LICENSE_${PN}-js-yaml = "MIT"
LICENSE_${PN}-json-buffer = "MIT"
LICENSE_${PN}-json-schema-traverse = "MIT"
LICENSE_${PN}-json5 = "Unknown"
LICENSE_${PN}-jsonfile = "MIT"
LICENSE_${PN}-keyv = "MIT"
LICENSE_${PN}-latest-version = "MIT"
LICENSE_${PN}-lazy-val = "Unknown"
LICENSE_${PN}-locate-path = "MIT"
LICENSE_${PN}-lowercase-keys = "MIT"
LICENSE_${PN}-lru-cache = "ISC"
LICENSE_${PN}-make-dir-semver = "ISC"
LICENSE_${PN}-make-dir = "MIT"
LICENSE_${PN}-mime = "MIT"
LICENSE_${PN}-mimic-response = "MIT"
LICENSE_${PN}-minimatch = "ISC"
LICENSE_${PN}-minimist = "MIT"
LICENSE_${PN}-ms = "MIT"
LICENSE_${PN}-normalize-package-data-hosted-git-info = "ISC"
LICENSE_${PN}-normalize-package-data-semver = "ISC"
LICENSE_${PN}-normalize-package-data = "Unknown"
LICENSE_${PN}-normalize-url = "MIT"
LICENSE_${PN}-once = "ISC"
LICENSE_${PN}-p-cancelable = "MIT"
LICENSE_${PN}-p-limit = "MIT"
LICENSE_${PN}-p-locate = "MIT"
LICENSE_${PN}-p-try = "MIT"
LICENSE_${PN}-package-json-semver = "ISC"
LICENSE_${PN}-package-json = "MIT"
LICENSE_${PN}-path-exists = "MIT"
LICENSE_${PN}-path-parse = "MIT"
LICENSE_${PN}-prepend-http = "MIT"
LICENSE_${PN}-pump = "MIT"
LICENSE_${PN}-punycode = "MIT"
LICENSE_${PN}-pupa = "MIT"
LICENSE_${PN}-rc = "Unknown MIT"
LICENSE_${PN}-read-config-file = "Unknown"
LICENSE_${PN}-registry-auth-token = "MIT"
LICENSE_${PN}-registry-url = "MIT"
LICENSE_${PN}-require-directory = "MIT"
LICENSE_${PN}-require-main-filename = "ISC"
LICENSE_${PN}-resolve = "MIT"
LICENSE_${PN}-responselike = "MIT"
LICENSE_${PN}-safer-buffer = "MIT"
LICENSE_${PN}-sanitize-filename = "Unknown"
LICENSE_${PN}-sax = "Unknown"
LICENSE_${PN}-semver = "ISC"
LICENSE_${PN}-semver-diff-semver = "ISC"
LICENSE_${PN}-semver-diff = "MIT"
LICENSE_${PN}-set-blocking = "ISC"
LICENSE_${PN}-signal-exit = "ISC"
LICENSE_${PN}-source-map = "Unknown"
LICENSE_${PN}-source-map-support = "MIT"
LICENSE_${PN}-spdx-correct = "Apache-2.0"
LICENSE_${PN}-spdx-exceptions = "Unknown"
LICENSE_${PN}-spdx-expression-parse = "MIT"
LICENSE_${PN}-spdx-license-ids = "Unknown"
LICENSE_${PN}-sprintf-js = "Unknown"
LICENSE_${PN}-stat-mode = "MIT"
LICENSE_${PN}-string-width-ansi-regex = "MIT"
LICENSE_${PN}-string-width-emoji-regex = "MIT"
LICENSE_${PN}-string-width-is-fullwidth-code-point = "MIT"
LICENSE_${PN}-string-width-strip-ansi = "MIT"
LICENSE_${PN}-string-width = "MIT"
LICENSE_${PN}-strip-ansi = "MIT"
LICENSE_${PN}-strip-json-comments = "MIT"
LICENSE_${PN}-supports-color = "MIT"
LICENSE_${PN}-temp-file = "Unknown"
LICENSE_${PN}-term-size = "MIT"
LICENSE_${PN}-to-readable-stream = "MIT"
LICENSE_${PN}-truncate-utf8-bytes = "Unknown"
LICENSE_${PN}-type-fest = "MIT"
LICENSE_${PN}-typedarray-to-buffer = "MIT"
LICENSE_${PN}-unique-string = "MIT"
LICENSE_${PN}-universalify = "MIT"
LICENSE_${PN}-update-notifier = "Unknown"
LICENSE_${PN}-uri-js = "Unknown"
LICENSE_${PN}-url-parse-lax = "MIT"
LICENSE_${PN}-utf8-byte-length = "Unknown"
LICENSE_${PN}-validate-npm-package-license = "Apache-2.0"
LICENSE_${PN}-which-module = "ISC"
LICENSE_${PN}-widest-line = "MIT"
LICENSE_${PN}-wrap-ansi-ansi-regex = "MIT"
LICENSE_${PN}-wrap-ansi-strip-ansi = "MIT"
LICENSE_${PN}-wrap-ansi = "MIT"
LICENSE_${PN}-wrappy = "ISC"
LICENSE_${PN}-write-file-atomic = "ISC"
LICENSE_${PN}-xdg-basedir = "MIT"
LICENSE_${PN}-y18n = "ISC"
LICENSE_${PN}-yallist = "ISC"
LICENSE_${PN}-yargs = "MIT"
LICENSE_${PN}-yargs-parser = "ISC"
BBCLASSEXTEND = "native"


NPM_INSTALL_DEV = "1"

do_install_append() {
    rm -f ${D}${nonarch_libdir}/node
}
