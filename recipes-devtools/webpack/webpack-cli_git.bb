# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=7c6802ed94ac83214d15a26008fa22a5"

SRC_URI = "git://github.com/webpack/webpack-cli.git;protocol=https"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "6fc4f79ef6af3837ba77498e0889027ccc66731f"

S = "${WORKDIR}/git"

# NOTE: no Makefile found, unable to determine what needs to be done

do_configure () {
	# Specify any needed configure commands here
	:
}

do_compile () {
	# Specify compilation commands here
	:
}

do_install () {
	# Specify install commands here
	:
}

BBCLASSEXTEND = "native"


NPM_INSTALL_DEV = "1"
