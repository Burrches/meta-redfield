FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

DEPENDS_append_class-target = " linux-libc-headers "

SRC_URI_append_class-target = " \
        file://1000-redfield-improve-focus.patch \
        file://2000-sdl-set-wmclass.patch \
        file://0001-usb-host-use-correct-altsetting-in-usb_host_ep_updat.patch \
"

# Uncomment this to enable trace events for debugging
# EXTRA_OECONF += " --enable-trace-backends=simple"
