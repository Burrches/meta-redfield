SUMMARY = "Desirable packages for network vms (common between ndvm, vpnvm, etc.)"
LICENSE = "MIT"

inherit packagegroup

RDEPENDS_${PN} = " \
    arp-ethers \
    bridge-utils \
    conntrack-tools \
    coreutils \
    ethtool \
    iptables \
    iptables-modules \
    packagegroup-netfilter \
    kernel-module-aegis128 \
    kernel-module-aegis128-aesni \
    kernel-module-crc32c-intel \
    kernel-module-crc32-pclmul \
    kernel-module-des3-ede-x86-64 \
    kernel-module-ghash-clmulni-intel \
    kernel-module-sha1-ssse3 \
    kernel-module-sha256-ssse3 \
    kernel-module-sha512-ssse3 \
    kernel-module-nft-masq \
    kernel-module-overlay \
    kernel-module-tun \
    kernel-module-xen-gntalloc \
    kernel-module-xen-gntdev \
    kernel-module-xen-netback \
    kernel-module-xen-netfront \
    kernel-module-xt-masquerade \
    util-linux-dmesg \
    util-linux-flock \
    util-linux-logger \
    xen-tools-devd \
    xen-tools-xenstore \
    xen-tools-scripts-network \
    xen-tools-volatiles \
    xen-tools-xencommons \
"

# DEBUG LOVE
RDEPENDS_${PN} += " \
    tcpdump \
"
