SUMMARY = "Desirable packages for redfield gui images"
LICENSE = "MIT"

inherit packagegroup

RDEPENDS_${PN} = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'redfield-dev', 'xterm', '', d)} \
    adwaita-icon-theme-cursors \
    adwaita-icon-theme-hires \
    dash-to-dock \
    dom0-gnome-configs \
    flat-remix-gnome \
    go-installer \
    hide-activities-button \
    no-hot-corner \
    packagegroup-fonts-truetype-core \
    packagegroup-gnome3-base \
"
