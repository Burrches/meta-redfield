DESCRIPTION = "NDVM Create Service"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd

SRC_URI = " \
    file://ndvm-create \
    file://ndvm-create.service \
"

do_install() {
    install -d ${D}${systemd_system_unitdir}
    install -d ${D}${bindir}

    install -m 0644 ${WORKDIR}/ndvm-create.service ${D}${systemd_system_unitdir}
    install -m 0755 ${WORKDIR}/ndvm-create ${D}${bindir}
}

RDEPENDS_${PN} = " \
    bash \
    go-netctl \
"

SYSTEMD_SERVICE_${PN} = "ndvm-create.service"
