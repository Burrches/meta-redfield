DESCRIPTION = "Auto-mount USB service"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd

SRC_URI = " \
    file://automount-usb \
    file://automount-usb@.service \
"

do_install() {
    install -d ${D}${systemd_system_unitdir}
    install -d ${D}${bindir}

    install -m 0644 ${WORKDIR}/automount-usb@.service ${D}${systemd_system_unitdir}
    install -m 0755 ${WORKDIR}/automount-usb ${D}${bindir}
}

RDEPENDS_${PN} = " \
    bash \
"

SYSTEMD_SERVICE_${PN} = "automount-usb@.service"
