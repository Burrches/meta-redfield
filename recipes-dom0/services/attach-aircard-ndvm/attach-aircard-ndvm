#!/bin/bash

kernel=$1

busnum=$(cat /sys/bus/usb/devices/$kernel/busnum)
devnum=$(cat /sys/bus/usb/devices/$kernel/devnum)

# Create the mobile ndvm if it doesn't exist
ndvm-create ndvm-mobile

# When this is called, the ndvm probably doesn't exist yet.
# Wait until it does, and then attach the device.
until xl domid ndvm-mobile 2>/dev/null; do
    sleep 0.5
done

domid=$(xl domid ndvm-mobile)

# If the NDVM isn't HVM, we can't attach USB devices to it.
if [[ "$(xenstore-read /libxl/$domid/type)" != "hvm" ]]; then
    echo "NDVM is not HVM, will not attach USB device"
    exit 0
fi

# This call might be happening before QEMU is ready. As a lazy
# hack, keep trying until it succeeds. The better way to do this
# would be to figure out the correct state information to query
# from the device model before attaching.
until xl usbdev-attach $domid hostbus=$busnum hostaddr=$devnum controller=2; do
    sleep 1
done
