DESCRIPTION = "Dom0 udev automount rules"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://11-automount-usb-storage.rules \
    file://77-aircard.rules \
    file://systemd-udevd.service.override.conf \
"

do_install () {
    install -d ${D}${sysconfdir}/udev/rules.d
    install -m 0644 ${WORKDIR}/11-automount-usb-storage.rules ${D}${sysconfdir}/udev/rules.d
    install -m 0644 ${WORKDIR}/77-aircard.rules ${D}${sysconfdir}/udev/rules.d

    install -d ${D}${sysconfdir}/systemd/system/systemd-udevd.service.d
    install -m 0644 ${WORKDIR}/systemd-udevd.service.override.conf ${D}${sysconfdir}/systemd/system/systemd-udevd.service.d/override.conf
}

RDEPENDS_${PN} += " \
    systemd \
    udev \
"
