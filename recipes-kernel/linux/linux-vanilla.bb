DEPENDS += "${@bb.utils.contains('ARCH', 'x86', 'elfutils-native', '', d)}"
DEPENDS += "openssl-native util-linux-native"

COMPATIBLE_MACHINE = "qemuarm|qemuarm64|qemux86|qemuppc|qemumips|qemumips64|qemux86-64|intel-corei7"
KMACHINE ?= "intel-corei7-64"
LINUX_VERSION ?= "5.10.67"

SRC_URI[sha256sum] = "8c5740aa782593b8fabf9b71c2800182d4fe965adabd3595634113d48cb98a82"

SRC_URI = " \
           https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-${LINUX_VERSION}.tar.xz \
           file://defconfig \
           file://redfield.scc \
           file://xen.cfg \
           file://usb-override.cfg \
           file://0001-dont-init-pmc-core-in-pv-guests.patch \
           file://0002-iwlwifi-disable-msix.patch \
           file://openxt-netfront-support-backend-relocate.patch \
"

PV = "${LINUX_VERSION}"

inherit kernel kernel-yocto

S = "${WORKDIR}/linux-${LINUX_VERSION}"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"
LICENSE="GPLv2"

