DESCRIPTION = "Installer GNOME Shell Configurations"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://dconf.d/dconf-user.conf \
    file://dconf-profile \
    file://background.png \
    file://installer-gui.desktop \
    file://xterm.desktop \
"

DEPENDS += " dconf-native "

FILES_${PN} += "${sysconfdir}/dconf/db/local"
FILES_${PN} += "${datadir}/redfield"
FILES_${PN} += "${datadir}/applications/installer-gui.desktop"
FILES_${PN} += "${datadir}/applications/xterm.desktop"

do_compile() {
    ${RECIPE_SYSROOT_NATIVE}/${bindir}/dconf compile ${WORKDIR}/local ${WORKDIR}/dconf.d
}

do_install() {
    install -d ${D}/${sysconfdir}/dconf/db
    install -m 0644 ${WORKDIR}/local ${D}/${sysconfdir}/dconf/db/local

    install -d ${D}/${sysconfdir}/dconf/profile
    install -m 0644 ${WORKDIR}/dconf-profile ${D}/${sysconfdir}/dconf/profile/local

    install -d ${D}/${datadir}/applications
    install -m 0644 ${WORKDIR}/xterm.desktop ${D}/${datadir}/applications/
    install -m 0644 ${WORKDIR}/installer-gui.desktop ${D}/${datadir}/applications/

    # set installer to autostart
    install -d ${D}/${sysconfdir}/xdg/autostart
    ln -sf ${datadir}/applications/installer-gui.desktop ${D}/${sysconfdir}/xdg/autostart/installer-gui.desktop

    install -d ${D}/${datadir}/redfield
    install -d ${D}/${datadir}/redfield/backgrounds
    install -m 0644 ${WORKDIR}/background.png ${D}/${datadir}/redfield/backgrounds/
}

