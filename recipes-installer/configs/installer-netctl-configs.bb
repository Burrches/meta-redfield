DESCRIPTION = "INSTALLER NETCTL CONFIGS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://netctl-back.env \
    file://netctl-front-wlan0.env \
"

do_install() {
    install -d ${D}${sysconfdir}/netctl
    install -m 0644 ${WORKDIR}/netctl-back.env ${D}${sysconfdir}/netctl
    install -m 0644 ${WORKDIR}/netctl-front-wlan0.env ${D}${sysconfdir}/netctl
}

RDEPENDS_${PN} = " \
    go-netctl-back \
    go-netctl-front \
    go-netctl \
"
