SUMMARY = "Yubikey Full Disk Encryption"
HOMEPAGE = "https://github.com/agherzan/yubikey-full-disk-encryption"

# TODO: Replace this with something upstreamed (currently exists in xenclient-oe)
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d92e60ee98664c54f68aa515a6169708"

DEPENDS += " \
    gnu-efi-native \
    openssl \
    openssl-native \
    util-linux-native \
"

RDEPENDS_${PN} += "bash"

PV = "14+git${SRCPV}"
SRCREV = "7482ed93fb3faaf6f86e40a1bfb4200e10eb751b"

SRC_URI = " \
    git://github.com/agherzan/${BPN}.git;branch=master \
    file://0001-enable-non-interactive-challeneges.patch \
    file://0001-Makefile-add-libdir-variable.patch \
"

export DESTDIR="${D}"

S = "${WORKDIR}/git"

FILES_${PN} += " \
    ${bindir} \
    ${libdir} \
    ${sysconfdir} \
"

CLEANBROKEN = "1"

do_install() {
    oe_runmake install
}
