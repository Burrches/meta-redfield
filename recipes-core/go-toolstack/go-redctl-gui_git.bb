SUMMARY = "Redfield redctl GUI"
HOMEPAGE = "http://www.gitlab.com/redfield/redctl"
LICENSE = "Apache-2.0"
GO_IMPORT = "gitlab.com/redfield/redctl"
REDCTL_ROOT = "${WORKDIR}/${PN}-${PV}/src/${GO_IMPORT}"
LIC_FILES_CHKSUM = "file://${REDCTL_ROOT}/LICENSE;md5=38f7323dc81034e5b69acc12001d6eb1"

DEPENDS = " \
    grpc \
    gotron-builder-native \
    webpack-cli-native \
    webpack-native \
    nodejs-native \
"

inherit go pkgconfig

SRC_URI = " \
    git://${REDFIELD_SOURCES_URL}/redctl.git;protocol=${REDFIELD_SOURCES_PROTOCOL};branch=${REDFIELD_BRANCH};destsuffix=${BPN}-${PV}/src/${GO_IMPORT} \
"

SRCREV = "${AUTOREV}"

GO_LINKSHARED = ""
GOBUILDMODE = 'exe'
CGO_CFLAGS += " --sysroot=${RECIPE_SYSROOT}"

RDEPENDS_${PN} += " \
    libxscrnsaver \
    libasound \
    cups \
    gconf \
    libnss-nis \
    libpcre \
    zlib \
    libselinux \
    libsystemd \
    krb5 \
    libbsd \
    lz4 \
    libk5crypto \
    keyutils \
"

do_compile() {
    GOTRON_BUILDER_DATA_DIR=${RECIPE_SYSROOT_NATIVE}${datadir}/gotron-builder \
    DESTDIR=${D} NPM_FLAGS=--only=production make -C ${REDCTL_ROOT} gui-bins || go clean -modcache

    # Until the -modcacherw flag is available, this is needed to clean GOPATH/pkg/mod
    go clean -modcache
}

do_install() {
    installdir=/opt/redctl \
    DESTDIR=${D} make -C ${REDCTL_ROOT}/js install
}

RDEPENDS_${PN} = "bash bash-completion"
RDEPENDS_${PN}-dev = "bash make"

INSANE_SKIP_${PN} = "file-rdeps already-stripped ldflags"

FILES_${PN} = "/opt/redctl"
