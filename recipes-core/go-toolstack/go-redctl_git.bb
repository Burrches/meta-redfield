SUMMARY = "Redfield Toolstack (redctl)"
HOMEPAGE = "http://www.gitlab.com/redfield/redctl"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=38f7323dc81034e5b69acc12001d6eb1"

DEPENDS = " \
    grpc \
"

inherit go go-mod pkgconfig systemd

GO_IMPORT = "gitlab.com/redfield/redctl"
SRC_URI = " \
    git://${REDFIELD_SOURCES_URL}/redctl.git;protocol=${REDFIELD_SOURCES_PROTOCOL};branch=${REDFIELD_BRANCH};destsuffix=${BPN}-${PV}/src/${GO_IMPORT} \
    file://redctld.service \
    file://redctld-shutdown.service \
    file://tmpfiles-redctld.conf \
"

SRCREV = "${AUTOREV}"

GO_LINKSHARED = ""
GOBUILDMODE = 'exe'

RDEPENDS_${PN} = "bash bash-completion"
RDEPENDS_${PN}-dev = "bash make"

do_install_append() {
    DESTDIR=${D} make -C src/${GO_IMPORT} install

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/redctld.service ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/redctld-shutdown.service ${D}${systemd_system_unitdir}

    install -d -m 755 ${D}${sysconfdir}/tmpfiles.d
    install -p -m 644 ${WORKDIR}/tmpfiles-redctld.conf ${D}${sysconfdir}/tmpfiles.d/
}

SYSTEMD_SERVICE_${PN} = "redctld.service redctld-shutdown.service"
