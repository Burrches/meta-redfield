SUMMARY = "Redfield netctl"
HOMEPAGE = "http://www.gitlab.com/redfield/netctl"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=38f7323dc81034e5b69acc12001d6eb1"

DEPENDS = " \
    grpc \
"

inherit go go-mod pkgconfig systemd

GO_IMPORT = "gitlab.com/redfield/netctl"
SRC_URI = " \
    git://${REDFIELD_SOURCES_URL}/netctl.git;protocol=${REDFIELD_SOURCES_PROTOCOL};branch=${REDFIELD_BRANCH};destsuffix=${BPN}-${PV}/src/${GO_IMPORT} \
"

SRCREV = "${AUTOREV}"

GO_LINKSHARED = ""
GOBUILDMODE = 'exe'

do_install_append() {
    oe_runmake DESTDIR=${D} -C src/${GO_IMPORT} install-systemd-configs install-udev-rules
    install -m 0755 ${B}/bin/* ${D}/${bindir}
}

# Split packages for netctl, netctl-front, netctl-back
PACKAGES += "\
    ${PN}-front \
    ${PN}-back \
"

FILES_${PN} = "\
    ${bindir}/netctl \
    ${sysconfdir}/bash_completion.d \
"

FILES_${PN}-front = "\
    ${nonarch_base_libdir}/udev \
    ${bindir}/netctl-front \
    ${systemd_system_unitdir}/netctl-front@.service \
    ${systemd_system_unitdir}/gen-netctl-front-env@.service \
    ${libexecdir}/gen-netctl-front-env \
"

FILES_${PN}-back = "\
    ${bindir}/netctl-back \
    ${sysconfdir} \
    ${systemd_system_unitdir}/netctl-back.service \
    ${systemd_system_unitdir}/gen-netctl-back-env.service \
    ${libexecdir}/gen-netctl-back-env \
"

SYSTEMD_PACKAGES += " \
    ${PN}-back \
    ${PN}-front \
"

SYSTEMD_SERVICE_${PN}-back = "netctl-back.service gen-netctl-back-env.service"
SYSTEMD_SERVICE_${PN}-front = "netctl-front@.service gen-netctl-front-env@.service"

# netctl-front@.service is invoked by udev rules.
SYSTEMD_AUTO_ENABLE_${PN}-front = "disable"

RDEPENDS_${PN} = "bash bash-completion"
RDEPENDS_${PN}-dev = "bash make"

RDEPENDS_${PN}-front += " \
    bash \
    modemmanager \
    usb-modeswitch \
"

RDEPENDS_${PN}-back += " \
    bash \
"

INSANE_SKIP_${PN}-back += "ldflags"
INSANE_SKIP_${PN}-front += "ldflags"
