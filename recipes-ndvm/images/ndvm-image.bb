SUMMARY = "A minimalist nat ndvm"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit redfield-image

IMAGE_FSTYPES = "ext4"

IMAGE_INSTALL += " \
    argo-module \
    go-netctl-front \
    redfield-image-configs \
    packagegroup-network-driver-modules \
    packagegroup-networkvm \
    ndvm-iptables-configs \
    ndvm-networkd-configs \
    ndvm-sysctl-configs \
    ndvm-modules-configs \
    pciutils \
    usbutils \
    linux-firmware \
    wpa-supplicant-cli \
    wpa-supplicant-passphrase \
"

ROOTFS_POSTPROCESS_COMMAND_append = " \
    image_postprocess_purge_default_certificates; \
"
