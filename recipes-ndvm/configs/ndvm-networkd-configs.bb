DESCRIPTION = "NDVM NETWORKD CONFIGS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://10-brbridged.netdev \
    file://10-brnat.netdev \
    file://10-mgmt0.link \
    file://10-outside0.link \
    file://10-vif.link \
    file://10-wlan0.link \
    file://10-wwan0.link \
    file://11-brnat.network \
    file://11-mgmt0.network \
    file://11-outside0.network \
    file://11-wlan0.network \
    file://12-brbridged.network \
"

do_install() {
    install -d ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-brbridged.netdev ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-brnat.netdev ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-mgmt0.link ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-outside0.link ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-vif.link ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-wlan0.link ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-wwan0.link ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/11-brnat.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/11-mgmt0.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/11-outside0.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/11-wlan0.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/12-brbridged.network ${D}${sysconfdir}/systemd/network
}

RDEPENDS_${PN} = " \
    bash \
    ethtool \
    wpa-supplicant \
"
